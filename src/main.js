import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueApexCharts from 'vue-apexcharts'
import VueRx from "vue-rx";
import VueSimpleContextMenu from 'vue-simple-context-menu'
Vue.use(VueRx);
Vue.use(VueApexCharts);
Vue.use(require('vue-moment'));
Vue.component('apexchart', VueApexCharts);
Vue.component('vue-simple-context-menu', VueSimpleContextMenu);
Vue.config.productionTip = false;

new Vue({
  router,
  render: function (h) { return h(App) }
}).$mount('#app');
